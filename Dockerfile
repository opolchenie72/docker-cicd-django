FROM python:3-alpine
WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
COPY . /app
RUN mkdir db
EXPOSE 8000
CMD sh init.sh && python manage.py runserver 0.0.0.0:8000
